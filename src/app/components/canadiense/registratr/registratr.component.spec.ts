import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistratrComponent } from './registratr.component';

describe('RegistratrComponent', () => {
  let component: RegistratrComponent;
  let fixture: ComponentFixture<RegistratrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistratrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistratrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
