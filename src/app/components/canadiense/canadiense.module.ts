import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarComponent } from './listar/listar.component';
import { ActualizarComponent } from './actualizar/actualizar.component';
import { RegistratrComponent } from './registratr/registratr.component';
import { CanadienseRoutingModule } from './canadiense-routing.module';



@NgModule({
  declarations: [ListarComponent, ActualizarComponent, RegistratrComponent],
  imports: [
    CommonModule,
    CanadienseRoutingModule
  ]
})
export class CanadienseModule { }
