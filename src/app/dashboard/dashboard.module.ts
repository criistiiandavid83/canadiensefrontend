import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavarComponent } from './navar/navar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './main/main.component';
import {SidebarModule} from 'primeng/sidebar';
import {ButtonModule} from 'primeng/button';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  declarations: [NavarComponent, SidebarComponent, MainComponent],
  imports: [
    CommonModule,
    SidebarModule,
    ButtonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
